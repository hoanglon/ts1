import org.junit.jupiter.api.*;

public class CalculatorTest {
    //ARRANGE
    static Calculator c;

    @BeforeAll
    public static void initVariable() {
        c = new Calculator();
    }

    @Test
    public void add_addNumbers_returns5(){
        int num1 = 3;
        int num2 = 2;
        int expectedResult = num1 + num2;

        int actualResult = c.add(3,2);

        Assertions.assertEquals(expectedResult, actualResult);
    }


    @Test
    public void subtract_subtractNumbers_returns5(){
        int num1 = 12;
        int num2 = 7;
        int expectedResult = num1 - num2;

        int actualResult = c.subtract(12,7);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void multiply_multiplyNumbers_returns20(){
        int num1 = 5;
        int num2 = 4;
        int expectedResult = num1 * num2;

        int actualResult = c.multiply(5,4);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void divide_divideNumbers_returns5(){
        int num1 = 10;
        int num2 = 2;
        int expectedResult = num1 / num2;

        int actualResult = c.divide(10,2);

        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void divide_divisionByZero_throwsException(){
        int num1 = 5;
        int num2 = 0;

        try {
            c.divide(num1, num2);
        } catch (ArithmeticException e){
            Assertions.assertEquals("/ by zero", e.getMessage());
        }
    }

    @Test
    public void exceptionThrownTest_exceptionThrown_exception() {

        // ACT + ASSERT 1 (Obsahuje assert, zda byla vyhozena výjimka očekávaného typu)
        Exception exception = Assertions.assertThrows(Exception.class, () -> c.exceptionThrown());


        String expectedMessage = "Ocekavana vyjimka";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }


}
