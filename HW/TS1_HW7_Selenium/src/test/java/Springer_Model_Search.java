import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Springer_Model_Search {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "all-words")
    private WebElement all_words;

    @FindBy(id = "exact-phrase")
    private WebElement exact_phrase;

    @FindBy(id = "least-words")
    private WebElement least_words;

    @FindBy(id = "without-words")
    private WebElement without_words;

    @FindBy(id = "title-is")
    private WebElement title_is;

    @FindBy(id = "author-is")
    private WebElement author_is;


    @FindBy(id = "date-facet-mode")
    private WebElement date_mode;

    @FindBy(id = "facet-start-year")
    private WebElement date_start;

    @FindBy(id = "facet-end-year")
    private WebElement date_end;

    @FindBy(id = "results-only-access-checkbox-advanced")
    private WebElement result_checkbox;

    @FindBy(id = "submit-advanced-search")
    public WebElement submit_advanced;

    public Springer_Model_Search(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        PageFactory.initElements(driver, this);

    }

    private void setAll_words(String words){
        all_words.sendKeys(words);
    }

    private void setExact_phrase(String phrase){
        exact_phrase.sendKeys(phrase);
    }

    private void setLeast_words(String leastWords){
        least_words.sendKeys(leastWords);
    }

    private void setWithout_words(String withoutWords){
        without_words.sendKeys(withoutWords);
    }

    private void setTitle_is(String title){
        title_is.sendKeys(title);
    }

    private void setAuthor_is(String author){
        author_is.sendKeys(author);
    }

    private void setDate_mode(String value){
        Select select = new Select(date_mode);
        select.selectByValue(value);
    }

    private void setDate_start(String start){
        date_start.sendKeys(start);
    }

    private void setDate_end(String end){
        date_start.sendKeys(end);
    }

    private void setResult_checkbox(boolean YesOrNo){
        if(YesOrNo == true && !result_checkbox.isSelected()){
            result_checkbox.click();
        }
        else if(YesOrNo == false && result_checkbox.isSelected()){
            result_checkbox.click();
        }
    }

    public void advanced_search(String words, String phrase, String leastWords, String withoutWords, String title, String author, String value, String start, String end, boolean YesOrNo){
        wait.until(ExpectedConditions.visibilityOf(submit_advanced));
        setAll_words(words);
        setExact_phrase(phrase);
        setLeast_words(leastWords);
        setWithout_words(withoutWords);
        setTitle_is(title);
        setAuthor_is(author);
        setDate_mode(value);
        setDate_start(start);
        setDate_end(end);
        setResult_checkbox(YesOrNo);
        submit_advanced.sendKeys(Keys.ENTER);
    }
}
