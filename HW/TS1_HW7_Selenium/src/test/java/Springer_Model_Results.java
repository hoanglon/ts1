import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Springer_Model_Results {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id = "content-type-facet")
    private WebElement content_types;

    @FindBy(id = "results-list")
    private WebElement results_list;


    public Springer_Model_Results(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        PageFactory.initElements(driver, this);
    }

    public void set_content_type(String type){
        wait.until(ExpectedConditions.visibilityOf(content_types));
        content_types.findElement(By.partialLinkText(type)).click();

    }

    public void access_nPlaced_result(int rank){
        wait.until(ExpectedConditions.visibilityOf(results_list));
        List <WebElement> headlines = results_list.findElements(By.tagName("h2"));
        WebElement Link = headlines.get(rank).findElement(By.tagName("a"));
        Link.click();
    }

}
