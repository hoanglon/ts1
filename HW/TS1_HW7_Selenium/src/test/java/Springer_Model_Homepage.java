import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Springer_Model_Homepage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(className = "register-link")
    private WebElement register_login_link;

    @FindBy(className = "open-search-options")
    private WebElement searchButton;

    @FindBy(id = "advanced-search-link")
    private WebElement advance_search_link;

    @FindBy(className = "cc-banner__footer")
    private WebElement cookies;

    public Springer_Model_Homepage(WebDriver driver){
        this.driver = driver;
        driver.get("https://link.springer.com/");
        wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        PageFactory.initElements(driver, this);
        click_on_cookies();
    }

    private void click_on_cookies(){
        cookies.findElement(By.tagName("button")).click();
    }

    public void go_to_login(){
        wait.until(ExpectedConditions.visibilityOf(register_login_link));
        register_login_link.click();

    }
    public void go_to_Search(){
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();
        advance_search_link.click();
    }
}
