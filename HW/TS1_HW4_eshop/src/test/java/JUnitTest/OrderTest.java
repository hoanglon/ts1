package JUnitTest;

import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {@Test
public void testConstructorOrder() {
    ShoppingCart cart = new ShoppingCart();
    Item item1 = new StandardItem(1, "item1", 10.0f, "category1", 1);
    Item item2 = new StandardItem(2, "item2", 20.0f, "category2", 2);
    cart.addItem(item1);
    cart.addItem(item2);
    String customerName = "Deez";
    String customerAddress = "Praha 123";
    int state = 1;
    Order order = new Order(cart, customerName, customerAddress, state);
    assertEquals(cart.getCartItems(), order.getItems());
    assertEquals(customerName, order.getCustomerName());
    assertEquals(customerAddress, order.getCustomerAddress());
    assertEquals(state, order.getState());

    Order order2 = new Order(cart, customerName, customerAddress);
    assertEquals(cart.getCartItems(), order2.getItems());
    assertEquals(customerName, order2.getCustomerName());
    assertEquals(customerAddress, order2.getCustomerAddress());
    assertEquals(0, order2.getState());
}
}
