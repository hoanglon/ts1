package JUnitTest;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Item;
import storage.ItemStock;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemStockTest {
    @Test
    public void testConstructorItemStock() {
        Item item = new Item(123, "Test Item", 10.0f, "Test Category");
        ItemStock itemStock = new ItemStock(item);

        assertEquals(item, itemStock.getItem());
        assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest(name = "Adding {0} Items and then Taking {1} Items away should be {2}")
    @CsvSource({"5,3,2", "1,3,-2", "0,0,0"})
    public void paramTestCountItemStock(int a, int b, int c) {

        ItemStock iStock = new ItemStock(new Item(123, "Test Item", 10.0f, "Test Category"));
        int expectedResult = c;

        iStock.IncreaseItemCount(a);
        iStock.decreaseItemCount(b);
        int result = iStock.getCount();

        assertEquals(expectedResult, result);
    }
}
