package JUnitTest;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StandardItemTest {
    @Test
    public void testConstructorStandardItem() {
        StandardItem item1 = new StandardItem(1, "Product 1", 10.0f, "Category 1", 5);

        assertEquals(1, item1.getID());
        assertEquals("Product 1", item1.getName());
        assertEquals(10.0f, item1.getPrice());
        assertEquals("Category 1", item1.getCategory());
        assertEquals(5, item1.getLoyaltyPoints());
    }

    @Test
    public void testConstructorAndCopyStandardItem() {
        StandardItem item2 = new StandardItem(2, "Product 2", 11.0f, "Category 2", 6);
        StandardItem item3 = item2.copy();

        assertEquals(item2.getID(), item3.getID());
        assertEquals(item2.getName(), item3.getName());
        assertEquals(item2.getPrice(), item3.getPrice());
        assertEquals(item2.getCategory(), item3.getCategory());
        assertEquals(item2.getLoyaltyPoints(), item3.getLoyaltyPoints());

    }

    @ParameterizedTest(name = "ItemID{0} should be {5}")
    @CsvSource({"1, Product 2, 11.0f, Category 2, 6 ,True", "6, Product 3, 1.0f, Drugs, 999 ,False"})
    public void paramTestEqualsStandardItem(int a, String b, float c, String d, int e, boolean f) {

        StandardItem sItem = new StandardItem(1, "Product 2", 11.0f, "Category 2", 6);
        boolean expectedResult = f;

        boolean result = sItem.equals(new StandardItem(a, b, c, d, e));

        assertEquals(expectedResult, result);
    }
}
