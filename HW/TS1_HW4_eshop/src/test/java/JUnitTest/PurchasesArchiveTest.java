package JUnitTest;

import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Item;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PurchasesArchiveTest {
    PurchasesArchive purchasesArchive;
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();


    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    public void testPrintItemPurchaseStatistics() {
        purchasesArchive = new PurchasesArchive();
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new StandardItem(1, "item1", 10.0f, "category1", 1);
        Item item2 = new StandardItem(2, "item2", 20.0f, "category2", 2);
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item2);
        String customerName = "Deez";
        String customerAddress = "Praha 123";
        int state = 1;
        purchasesArchive.putOrderToPurchasesArchive(new Order(cart, customerName, customerAddress, state));
        String expectedOutput = "Item with ID 1 added to the shopping cart.\r\n" +
                "Item with ID 2 added to the shopping cart.\n" +
                "Item with ID 2 added to the shopping cart.\n" +
                "ITEM PURCHASE STATISTICS:\n" +
                "ITEM  Item   ID 1   NAME item1   CATEGORY category1   PRICE 10.0   LOYALTY POINTS 1   HAS BEEN SOLD 1 TIMES\n" +
                "ITEM  Item   ID 2   NAME item2   CATEGORY category2   PRICE 20.0   LOYALTY POINTS 2   HAS BEEN SOLD 2 TIMES\n";
        purchasesArchive.printItemPurchaseStatistics();
        assertEquals(expectedOutput, outContent.toString());
    }


    @Test
    public void testPutOrderToPurchasesArchive() {
        purchasesArchive = new PurchasesArchive();
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new StandardItem(1, "item1", 10.0f, "category1", 1);
        Item item2 = new StandardItem(2, "item2", 20.0f, "category2", 2);
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item2);
        String customerName = "Deez";
        String customerAddress = "Praha 123";
        int state = 1;
        Order order = new Order(cart, customerName, customerAddress, state);
        purchasesArchive.putOrderToPurchasesArchive(order);

        assertEquals(order, purchasesArchive.orderArchive.get(0));


    }

    @Test
    public void testConstructorItemPurchaseArchive() {
        StandardItem item = new StandardItem(2, "Product 2", 11.0f, "Category 2", 6);
        ItemPurchaseArchiveEntry itemEntry = new ItemPurchaseArchiveEntry(item);

        assertEquals(item, itemEntry.refItem);
        assertEquals(1, itemEntry.soldCount);

    }

    @Test
    public void testGetHowManyTimesHasBeenItemSold() {
        purchasesArchive = new PurchasesArchive();
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new StandardItem(1, "item1", 10.0f, "category1", 1);
        Item item2 = new StandardItem(2, "item2", 20.0f, "category2", 2);
        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item2);
        String customerName = "Deez";
        String customerAddress = "Praha 123";
        int state = 1;
        Order order = new Order(cart, customerName, customerAddress, state);
        purchasesArchive.putOrderToPurchasesArchive(order);

        assertEquals(2, purchasesArchive.getHowManyTimesHasBeenItemSold(item2));

    }
}
