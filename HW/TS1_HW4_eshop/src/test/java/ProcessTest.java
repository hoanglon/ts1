import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Order;
import shop.*;
import storage.NoItemInStorage;


public class ProcessTest {

    ShoppingCart cart;

    Item[] storageItems;

    @BeforeEach
    public void setup() throws NoItemInStorage {

        EShopController.startEShop();
        int[] itemCount = {1,2};
        storageItems = new Item[2];
        storageItems [0] = new StandardItem(1, "Dancing Panda v.2", 5000, "GADGETS", 5);
        storageItems [1] = new StandardItem(2, "Dancing Panda v.3 with USB port", 6000, "GADGETS", 10);
        if (EShopController.getStorage().getItemCount(storageItems[0].getID()) != 0 || EShopController.getStorage().getItemCount(storageItems[1].getID()) != 0) {
            EShopController.getStorage().removeItems(storageItems[0], EShopController.getStorage().getItemCount(storageItems[0].getID()));
            EShopController.getStorage().removeItems(storageItems[1], EShopController.getStorage().getItemCount(storageItems[1].getID()));
        }

        for (int i = 0; i < storageItems.length; i++) {
            EShopController.getStorage().insertItems(storageItems[i], itemCount[i]);
        }
        EShopController.getCarts().clear();
        cart = EShopController.newCart();
    }


    @Test
    @Order(1)
    public void ProcessTest1_enoughItems_ProcessSucceeds() throws NoItemInStorage {
        //check of storage
        Assertions.assertEquals(storageItems.length, EShopController.getStorage().getStockEntries().toArray().length);

        //check if cart item corresponds
        cart.addItem(storageItems[0]);
        cart.addItem(storageItems[1]);
        Assertions.assertEquals (storageItems[0], EShopController.getCarts().get(0).getCartItems().get(0));
        Assertions.assertEquals (storageItems[1], EShopController.getCarts().get(0).getCartItems().get(1));

        cart.removeItem(storageItems[1].getID());
        Assertions.assertEquals (storageItems[0], EShopController.getCarts().get(0).getCartItems().get(0));
        Assertions.assertEquals(2, EShopController.getCarts().get(0).getItemsCount());

        //sends order
        EShopController.purchaseShoppingCart(cart, "Vojtech Kaspar", "Praha");

        //check if order was processed in storage
        Assertions.assertEquals(0, EShopController.getStorage().getItemCount(storageItems[0]));

        //checks if order was put to archive
        Assertions.assertEquals(1, EShopController.getArchive().getHowManyTimesHasBeenItemSold(storageItems[0]));

    }

    @Test
    @Order(2)
    public void ProcessTest2_NotEnoughItemsInStorage_ProcessThrowException(){
        //check of storage
        Assertions.assertEquals(storageItems.length, EShopController.getStorage().getStockEntries().toArray().length);

        //check if cart item corresponds
        cart.addItem(storageItems[0]);
        cart.addItem(storageItems[0]);
        Assertions.assertEquals(2, EShopController.getCarts().get(0).getItemsCount());

        Exception exception = Assertions.assertThrows(Exception.class, () -> EShopController.purchaseShoppingCart(cart, "Vojtech Kaspar", "Praha"));
        String expectedMessage = "No item in storage";
        String actualMessage = exception.getMessage();
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
