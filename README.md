## **BRANCHES**
- MAIN

    - HW
                - Domácí úkoly rozdělený do složek pojmenovány podle vzorce: Předmět_ČísloÚkolu_JménoÚkolu
    - CV
                - Úkoly/aktivita na cvičení pojmenovány podle vzorce: Předmět_ČísloCvičení_JménoÚkolu

- Factorial
            - 1. domácí úkol, větev je mergnutá s MAIN

- Semestralka
            - V této větvi bude uložena semestrální práce tohoto projektu

## **WIKI** 

V této části bude dokumentace k Semestrální práci
